# FAL-2144 Sprint planning checklist and automation (planning and tickets)

https://tasks.opencraft.com/browse/FAL-2144

This repository is not for code; this is for planning and managing tickets publicly.

This will contain:

- Tickets descriptions: in GitLab issues
- Proposals and discoveries: in GitLab pull requests
- Documents: in repo as markdown documents
