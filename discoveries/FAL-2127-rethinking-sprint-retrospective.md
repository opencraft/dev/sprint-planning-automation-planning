## Background

After a long synchronous discussion, it was decided that the previous implementation of the Sprint Retrospective and Evaluation in Monday.com were not as useful as they could've been. As a result, a new process was discussed: Continue using Monday.com to collect the sprint retrospective of the cell members, make the voting and at the end of the sprint, fully automatically, obtain all that list, process it (perhaps obtain the most voted) and post it on the forum.
To do this, coding tasks are needed in Crafty Bot to obtain the list of the sprint retrospective, make a connection with the Discourse API, create the post using that API and automate all of the above at some point at the end of the sprint. In addition, tasks are needed to discuss how the list of the retrospective will be processed (if only the 3 or 5 most voted will be posted or the entire list?, etc), and the way in which they will be posted (will we post separately for each cell or in the shared thread?, etc).

## Work estimation

### Announce the new process for the sprint retrospective ([#6](https://gitlab.com/opencraft/dev/sprint-planning-automation-planning/-/issues/6) - [FAL-2213](https://tasks.opencraft.com/browse/FAL-2213))

In order to perform the task, it is necessary to pick a way on how the Sprint Retrospective items will be processed before being published to the forum. Also, is necessary to announce this new process for the sprint retrospective in the forum and get feedback from the team.

Estimate: 2 - 6 hours

### Integrate Crafty Bot with the Discourse API ([#4](https://gitlab.com/opencraft/dev/sprint-planning-automation-planning/-/issues/4) - [FAL-2214](https://tasks.opencraft.com/browse/FAL-2214))

In Crafty Bot we would have to perform and configure an integration with the Discourse API. A python package that performs the integration was reviewed and can help us speed up the task:
https://github.com/bennylope/pydiscourse
For this, the following must be taken into account:
- At least the way to make a post in a thread must be implemented.
- The configuration should be such that credentials can be changed easily.
- Documentation of this configuration.
- This integration must be implemented in such a way that it can be used in other functionalities in the future.

Estimate: 12 - 16 hours

### Process and publish Monday's Sprint Retrospective Board items ([#5](https://gitlab.com/opencraft/dev/sprint-planning-automation-planning/-/issues/5) - [FAL-2215](https://tasks.opencraft.com/browse/FAL-2215))

In Crafty Bot we would have to add a function to obtain the data from the Sprint Retrospective board, recreate the board, process it as discussed in the first task and post it in the forum in the corresponding thread.
For this, the following must be taken into account:
- The function must be able to be called by the Crafty Bot api (similar to what is done to create the Sprint Checklist)
- The forum thread where we are going to post must be configurable.
- The function must re-create the Sprint Retrospective board (or delete all items) to prepare for the next sprint.

Estimate: 16 - 24 hours

